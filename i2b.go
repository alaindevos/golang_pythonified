// @ Alain De Vos 
package main
//
import(
"os"
"log"
"bufio"
"fmt"
"strings"
)
//
func main(){
    inname:=os.Args[1]
    fmt.Println("Infile:",inname)
    outname:=os.Args[2]
    fmt.Println("Outfile:",outname)
    infile,inerr:=os.Open(inname)
    if inerr != nil {
        log.Fatal(inerr)
    }
    defer infile.Close()
    outfile,outerr:=os.Create(outname)
    if outerr != nil {
        log.Fatal(outerr)
    }
    defer outfile.Close()
    scanner := bufio.NewScanner(infile)
    linenumber:=0
    previousnumberofspaces:=0
    endingnumberofspaces:=0
    for scanner.Scan() {
        linenumber=linenumber+1
        fmt.Println("Line:",linenumber)
        aline:=scanner.Text()
        fmt.Println(aline)
        bline:=aline
        numberofspaces:=0
        breaklabel:
        for i := 0; i < len(bline); i++ {
            achar:=string(bline[i])
            if (achar==" ") {
                numberofspaces=numberofspaces+1
            }
            if achar!=" "{
                break breaklabel
            }
        }
        fmt.Println("Numberofspaces",numberofspaces)
        if (numberofspaces%4) !=0{
            fmt.Println("Wrongnumberofspaces line",linenumber)
        }
        endingnumberofspaces=numberofspaces
        myident:=(numberofspaces-previousnumberofspaces)/4
        fmt.Println("Myident:",myident)
        if(myident<0){
            s:=strings.Repeat("}",-myident)
            fmt.Fprintf(outfile,"\n"+s+"\n")
            fmt.Fprintf(outfile,bline)
        } else if(myident>0){
            s:=strings.Repeat("{",myident)
            fmt.Fprintf(outfile,s)
            fmt.Fprintf(outfile,"\n"+bline)
        } else {
            fmt.Fprintf(outfile,"\n"+bline)
        }
        previousnumberofspaces=numberofspaces
    }
    fmt.Println("Endingnumberofspaces",endingnumberofspaces)
    myident:=-endingnumberofspaces/4
    if(myident<0){
            s:=strings.Repeat("}",-myident)
            fmt.Fprintf(outfile,"\n"+s+"\n")
    }  
}