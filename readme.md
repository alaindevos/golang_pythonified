# i2b

i2b , short for ident to curly braces, is a preprocessor which allows golang programs to be written with python indentation style instead of curly braces.  
So we have "pythonified" golang.

### i2b usage:
i2b inputfile.go outputile.go
